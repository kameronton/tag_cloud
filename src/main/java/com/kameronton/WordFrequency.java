package com.kameronton;

/**
 * Created by kameron on 21.03.2017.
 */
public class WordFrequency implements Comparable<WordFrequency>{
    private String word;
    private int frequency;

    public WordFrequency(String word, int frequency){
        this.word = word;
        this.frequency = frequency;
    }

    public String getWord() {
        return word;
    }

    public int getFrequency() {
        return frequency;
    }

    @Override
    public int compareTo(WordFrequency o) {
        return o.frequency - frequency;
    }

    @Override
    public String toString() {
        return word;
    }
}
