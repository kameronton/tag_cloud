package com.kameronton;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException{
        Path output = Paths.get("output.txt"); //get list of files
        Analyzer analyzer = new Analyzer();
        List<String> words = new ArrayList<>();

        List<WordFrequency> cloud = analyzer.load("input.txt"); //get sorted list of words with frequencies
        for (WordFrequency word: cloud) //extract words from list
            words.add(word.getWord());
        Files.write(output, words);
    }
}
