package com.kameronton;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by kameron on 21.03.2017.
 */
public class Analyzer {

    public List<String> readFiles(String path) throws IOException{ //read text from several files
        List<String> files = Files.readAllLines(Paths.get(path));
        List<String> allLines = new ArrayList<>();
        for(String file: files)
            allLines.addAll(readFile(file));
        return allLines;
    }

    public List<String> readFile(String path) throws IOException{
        return Files.readAllLines(Paths.get(path));
    }

    public Map<String, Integer> countFrequencies(List<String> lines){

        Map<String,Integer> frequencies = new HashMap<>();

        for(String line: lines){ //tokenize lines
            List<String> words = Arrays.asList(line.replaceAll("[^A-Za-z0-9\\s]", " ").split("\\s+"));

            for(String word: words){
                word = word.toLowerCase(); //normalize words
                if (!frequencies.containsKey(word))
                    frequencies.put(word,1);
                else
                    frequencies.put(word, frequencies.get(word)+1);
            }
        }
        return frequencies;
    }

    public List<WordFrequency> sortFrequencies(Map<String, Integer> frequencies){
        List<WordFrequency> wordFrequencies = new ArrayList<>();

        for(Map.Entry<String, Integer> frequency: frequencies.entrySet())
            wordFrequencies.add(new WordFrequency(frequency.getKey(), frequency.getValue()));

        Collections.sort(wordFrequencies);
        return wordFrequencies;
    }

    public List<WordFrequency> load(String path) throws IOException{
        return  sortFrequencies(countFrequencies(readFiles(path)));
    }
}
